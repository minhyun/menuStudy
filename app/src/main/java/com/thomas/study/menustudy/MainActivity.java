package com.thomas.study.menustudy;

import android.content.Intent;
import android.net.Uri;
import android.support.v4.app.FragmentTabHost;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.ShareActionProvider;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.net.URI;

public class MainActivity extends AppCompatActivity {

    private ShareActionProvider mShareActionProvider;
    private FragmentTabHost mTabHost;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mTabHost=(FragmentTabHost)findViewById(android.R.id.tabhost);
        mTabHost.setup(this,getSupportFragmentManager(),android.R.id.tabcontent);

        //tabspec의 이름은 "1"이고 (tag).화면에 보여질글 "11", 나타낼 fragment ,전달할 bundle값
        mTabHost.addTab(mTabHost.newTabSpec("1").setIndicator("11"), Fragment1.class,null);
        mTabHost.addTab(mTabHost.newTabSpec("2").setIndicator("22"), Fragment2.class,null);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        //getMenuInflater().inflate(R.menu.menu,menu);

/*        //android.support.v7.widget.SearchView 사용
        MenuItem sItem = menu.findItem(R.id.imageitem1);
        SearchView view = (SearchView)MenuItemCompat.getActionView(sItem);
        view.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                Toast.makeText(MainActivity.this, query, Toast.LENGTH_SHORT).show();
                return true;
            }
            @Override
            public boolean onQueryTextChange(String newText) {
                return false;
            }
        });*/

/*        //커스텀 serchview
        MenuItem item =menu.findItem(R.id.imageitem2);
        View actionView = MenuItemCompat.getActionView(item);
        final EditText keywordView = (EditText)actionView.findViewById(R.id.action_et);
        Button btn = (Button)actionView.findViewById(R.id.action_btn);

        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String keyword = keywordView.getText().toString();
                Toast.makeText(MainActivity.this, keyword, Toast.LENGTH_SHORT).show();
            }
        });*/

    /*    MenuItem shareItem = menu.findItem(R.id.action_share);
        mShareActionProvider =(ShareActionProvider)MenuItemCompat.getActionProvider(shareItem);
        mShareActionProvider.setShareIntent(getDefaultIntent());*/

        return true;
    }

    private Intent getDefaultIntent(){

        String url ="http://www.naver.com";
        Intent intent = new Intent(Intent.ACTION_SEND);
        intent.setType("image/*");
        //intent.setData(Uri.parse(url));
        return intent;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
/*            case R.id.imageSubeItem1:
                Toast.makeText(this, "imageSubClick1", Toast.LENGTH_SHORT).show();
                return true;
            case R.id.imageSubeItem2:
                Toast.makeText(this, "imageSubClick2", Toast.LENGTH_SHORT).show();
                return true;
            case R.id.imageSubeItem3:
                Toast.makeText(this, "imageSubClick3", Toast.LENGTH_SHORT).show();
                return true;*/
            case R.id.textiSubItem1 :
                Toast.makeText(this, "메뉴1 클릭", Toast.LENGTH_SHORT).show();
                return true;
            case R.id.textiSubItem2 :
                Toast.makeText(this, "메뉴2 클릭", Toast.LENGTH_SHORT).show();
                return true;
            case R.id.textiSubItem3 :
                Toast.makeText(this, "메뉴3 클릭", Toast.LENGTH_SHORT).show();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
