package com.thomas.study.menustudy;


import android.os.Bundle;

import android.support.v4.app.Fragment;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.widget.PopupMenu;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;


/**
 * A simple {@link Fragment} subclass.
 */
public class Fragment1 extends Fragment {



    public Fragment1() {
        // Required empty public constructor
        this.setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v=inflater.inflate(R.layout.fragment_fragment1, container, false);
        Button btn =(Button)v.findViewById(R.id.fragment1_menu_btn);
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                PopupMenu popupMenu = new PopupMenu(getActivity(),view);
                popupMenu.inflate(R.menu.popupmenu);
                popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                        switch (item.getItemId()){
                            case R.id.textiSubItem1:
                                Toast.makeText(getActivity(), "1", Toast.LENGTH_SHORT).show();
                                return true;
                            case R.id.textiSubItem2:
                                Toast.makeText(getActivity(), "2", Toast.LENGTH_SHORT).show();
                                return true;
                            case R.id.textiSubItem3:
                                Toast.makeText(getActivity(), "3", Toast.LENGTH_SHORT).show();
                                return true;
                        }


                        /*int id = item.getItemId();
                        if(id==R.id.textiSubItem1){
                            Toast.makeText(getActivity(), "1", Toast.LENGTH_SHORT).show();
                            return true;
                        }*/
                        return false;
                    }
                });
                popupMenu.show();
            }
        });

        return v;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        getActivity().getMenuInflater().inflate(R.menu.fragment1,menu);
        MenuItem item =menu.findItem(R.id.imageitem2);
        View actionView = MenuItemCompat.getActionView(item);
        final EditText keywordView = (EditText)actionView.findViewById(R.id.action_et);
        Button btn = (Button)actionView.findViewById(R.id.action_btn);

        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String keyword = keywordView.getText().toString();
                Toast.makeText(getActivity(), keyword, Toast.LENGTH_SHORT).show();
            }
        });
    }
}